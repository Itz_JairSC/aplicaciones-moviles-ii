package com.example.compejs;

import java.util.List;

public class Mostrar_Noticias {

    public String estado;
    public List<Noticias> detalle;

    public Mostrar_Noticias() {
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Noticias> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Noticias> detalle) {
        this.detalle = detalle;
    }
}
