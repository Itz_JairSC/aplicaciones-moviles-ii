package com.example.compejs;

import java.util.List;

public class Detalles_disco {
    public  int discoId;
    public String estado;
    public List<Discos> disco;

    public int getDiscoId() {
        return discoId;
    }

    public void setDiscoId(int discoId) {
        this.discoId = discoId;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Discos> getDisco() {
        return disco;
    }

    public void setDisco(List<Discos> disco) {
        this.disco = disco;
    }
}
