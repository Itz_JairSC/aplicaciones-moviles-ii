package com.example.compejs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Inicio extends AppCompatActivity {
ListView lv;
Button btnMostrar;
Button btnCerrarS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        lv = (ListView) findViewById(R.id.ListaUsuarios);
        btnMostrar = (Button) findViewById(R.id.MostrarUsers);
        btnMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion service = Api.getApi(Inicio.this).create(ServicioPeticion.class);
                Call<Peticion_discos> mostrarinfo = service.getDiscos();
                mostrarinfo.enqueue(new Callback<Peticion_discos>() {
                    @Override
                    public void onResponse(Call<Peticion_discos> call, Response<Peticion_discos> response) {
                        Peticion_discos peticion = response.body();
                        if(response.body()== null){
                            Toast.makeText(Inicio.this, "Ocurrió un error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if(peticion.estado =="true"){
                            ArrayList<String> ListaName = new ArrayList<>();
                            final ArrayList<Integer> ListaId = new ArrayList<>();
                            for(int i = 0; i<peticion.discos.size()-1;i++){
                                ListaName.add(peticion.discos.get(i).nombre);
                                ListaId.add(peticion.discos.get(i).id);
                            }
                            ArrayAdapter<String> adaptador = new ArrayAdapter<>(Inicio.this,android.R.layout.simple_list_item_1,ListaName);
                            //adaptador = new ArrayAdapter(Inicio.this,android.R.layout.simple_list_item_1,ListaName);
                            lv.setAdapter(adaptador);

                            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                                    ServicioPeticion service = Api.getApi(Inicio.this).create(ServicioPeticion.class);
                                    Call<Detalles_disco> CallDetalles = service.getDetalles(ListaId.get(i));
                                    CallDetalles.enqueue(new Callback<Detalles_disco>() {
                                        @Override
                                        public void onResponse(Call<Detalles_disco> call, Response<Detalles_disco> response) {
                                            Detalles_disco peticion = response.body();
                                            if(peticion.estado == "true"){

                                                Intent intentRe = new Intent(Inicio.this, DetallesUsuario.class);
                                                intentRe.putExtra("texto1",peticion.disco.get(i).nombre);
                                                intentRe.putExtra("texto2",peticion.disco.get(i).album);
                                                intentRe.putExtra("texto3",peticion.disco.get(i).anio);
                                                startActivity(intentRe);
                                                Toast.makeText(Inicio.this,"Detalles del disco cargados correctamente",Toast.LENGTH_LONG).show();
                                                /*Intent intent = new Intent(Inicio.this,MainActivity.class);
                                                startActivity(intent);*/
                                            }
                                            else{

                                                Toast.makeText(Inicio.this,"Verifica tus datos",Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Detalles_disco> call, Throwable t) {
                                            Toast.makeText(Inicio.this, "Error inesperado", Toast.LENGTH_LONG).show();
                                            return;
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            Toast.makeText(Inicio.this, "Error del servidor", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Peticion_discos> call, Throwable t) {
                        Toast.makeText(Inicio.this, "Error inesperado", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        btnCerrarS = (Button) findViewById(R.id.btnCerrarS);
        btnCerrarS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN","");
                editor.commit();
                Toast.makeText(Inicio.this, "Ha serrado sesión satisfactoriamente", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Inicio.this,MainActivity.class);
                startActivity(intent);
            }
        });


    }


}
