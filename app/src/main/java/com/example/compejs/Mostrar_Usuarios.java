package com.example.compejs;

import java.util.List;

public class Mostrar_Usuarios {
public String estado;
public List<Usuarios> usuarios;

    public Mostrar_Usuarios() {
    }

    public List<Usuarios> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuarios> usuarios) {
        this.usuarios = usuarios;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


}
